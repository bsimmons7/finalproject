// Game Varibles 
PImage unicorn, space;
float unicornx, unicorny;
int rad; // controls character 
boolean play = false; // calls to game menu
int spacex, spacey, movex, movey; // set so background and character moves
int drawRect = 0; // pipes 
int GameResult = 0; // sets the game to win/ lose
Screen intro = new Screen (); // calls to menu tab

//***** Levels Variables*******//
Pipe1 pipe1 = new Pipe1(); //creates the pipe classes
Pipe2 pipe2 = new Pipe2();
Pipe3 pipe3 = new Pipe3();
Pipe4 pipe4 = new Pipe4();
float rectXL = 1200; //rectangle's starting x axis
float rectXB = 1200; 
float rectXW = 1200;
float rectXP = 1200;
import ddf.minim.*; // making minim active
Minim minim;
AudioPlayer player;
void setup()
{
  size(1200, 800);
  space = loadImage("./img/backg.png"); // Imports background image 
  unicorn = loadImage("./img/CH1.png"); // // Imports character file 
  movex = 1; // gravity for the character
  unicornx = width/2; //horizontal movement
  unicorny = height/2; // vertical movment
 minim = new Minim(this);
player = minim.loadFile("peritune-spook4.mp3"); // importing sound
player.play();// making sound active
}

void draw()
{     
  if (GameResult == 0) { // starts loop so if images pop up the game starts
    setBg();
    image(unicorn, unicornx, unicorny); //makes character pop up on screen
    ellipse(unicornx, unicorny, 1*rad, 1*rad); 
    pipe1.display(); // calls to pipe tabs
 }
 
 else {
  text("TRY AGAIN", 350, 500); // promps user that they lost
  fill (50, 55, 70); // text color
}
{ 
 text("Welcome to Flap Magic: Click to Play", 350, 90);
textSize(26);  
   }
//*** Mouse and Key Inputs***///
if (mouseButton ==  LEFT){   // Causes a reaction to mouse being pushed 
 intro.display();       // calls to menu screen
    }
    
  }
 
 

 


void keyPressed() 
// Character movments. 20 is the speed of the character//
{
  if      (keyCode == UP    && unicorny-rad >= 2    )  unicorny -=20; 
  else if (keyCode == DOWN  && unicorny+rad <= height)  unicorny +=20;
  else if (keyCode == LEFT  && unicornx-rad >= 2     )  unicornx -=20;
  else if (keyCode == RIGHT && unicornx+rad <= width )  unicornx +=20;
} 
// Set up for background for movments 
void setBg() 
{
  image(space, spacex, spacey);
  image(space, spacex + space.width, spacey);
  spacex = spacex - 2; // controlls speed 
  if (spacex < -space.width) {  

    spacex = 0; 
  }
}  

